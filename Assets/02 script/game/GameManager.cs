﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : FsmManager<GameState>
{
	public static GameManager _me;
	public static GameManager me
	{
		get { return _me; }
	}
	public ReadyUI readyUI;
	public PlayUI playUI;
	public EndUI endUI;

	public GroundManager groundManager;
	public Player player;

	public float speed;
	public int score;	

	private void Awake()
	{
		_me = this;
		InitializeState();
	}

	protected override void InitializeState()
	{
		AddState(new GameStateReady(this));
		AddState(new GameStatePlay(this));
		AddState(new GameStateEnd(this));
		state = GameState.ready;
	}
	
	private void Update()
	{
		UpdateState(Time.deltaTime);
	}

	public void ShowReadyUI()
	{
		readyUI.SetActive(true);
		playUI.SetActive(false);
		endUI.SetActive(false);
	}
	public void ShowPlayUI()
	{
		readyUI.SetActive(false);
		playUI.SetActive(true);
		endUI.SetActive(false);
	}
	public void ShowEndUI()
	{
		readyUI.SetActive(false);
		playUI.SetActive(false);
		endUI.SetActive(true);
	}

	public void OnClickRight()
	{
		player.MoveRight();
	}
	public void OnClickLeft()
	{
		player.MoveLeft();
	}

	public void EndGame()
	{
		state = GameState.end;
	}
	public void OnClickRestart()
	{
		state = GameState.ready;
	}
	public void OnClickExit()
	{
		Application.Quit();
	}
}
