﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	private GameManager _gameManager;
	public GameManager gameManager
	{
		get
		{
			if (_gameManager == null)
				_gameManager = GameManager.me;

			return _gameManager;
		}
	}

	public List<GameObject> prefs;
	public Dictionary<int, List<Enemy>> pool = new Dictionary<int, List<Enemy>>();
	public Dictionary<int, int> poolIndex = new Dictionary<int, int>();

	private float spawnTimer = 0;
	private void Start()
	{
		for(int i=0; i<prefs.Count; ++i)
		{
			if (!pool.ContainsKey(i))
			{
				pool.Add(i, new List<Enemy>());
				poolIndex.Add(i, 0);
			}	

			for(int j=0; j<10; ++j)
			{
				GameObject go = Instantiate(prefs[i]);
				Enemy e = go.AddComponent<Enemy>();
				e.transform.SetParent(transform);
				e.gameObject.SetActive(false);
				pool[i].Add(e);
			}
		}
	}
	private void Update()
	{
		if (gameManager.state == GameState.end)
			return;

		spawnTimer += Time.deltaTime;
		if (spawnTimer < GameValue.enemyInterval)
			return;

		spawnTimer -= GameValue.enemyInterval;
		DoSpawn();
	}
	private void DoSpawn()
	{
		int key = Random.Range(1, 31);
		for (int x = -2; x <= 2; ++x)
		{
			bool doSpawn = key % 2 == 1;
			key /= 2;

			if (doSpawn)
			{
				int idx = Random.Range(0, pool.Keys.Count);
				Enemy e = SpawnEnemy(idx);
				e.transform.localPosition = new Vector3(x * GameValue.lineWidth, 0, 0);
			}
		}
	}
	private Enemy SpawnEnemy(int idx)
	{
		if (pool[idx][poolIndex[idx]].gameObject.activeSelf)
		{
			GameObject go = Instantiate(prefs[idx]);
			Enemy e = go.AddComponent<Enemy>();
			e.transform.SetParent(transform);
			e.gameObject.SetActive(false);
			pool[idx].Insert(++poolIndex[idx], e);
		}

		Enemy enemy = pool[idx][poolIndex[idx]];
		enemy.gameObject.SetActive(true);
		poolIndex[idx] = (poolIndex[idx] + 1) % pool[idx].Count;
		return enemy;
	}
}
