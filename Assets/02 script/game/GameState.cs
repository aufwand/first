﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
	ready,
	play,
	end,
}

public class GameStateBase : FsmState<GameState>
{
	protected GameManager manager;
	public GameStateBase(GameManager manager, GameState state) : base(state)
	{
		this.manager = manager;
	}
}

public class GameStateReady : GameStateBase
{
	public GameStateReady(GameManager manager) : base(manager, GameState.ready) { }
	public override void OnEnter()
	{
		base.OnEnter();
		manager.speed = GameValue.lineWidth * 3;
		manager.player.Init();
		manager.ShowReadyUI();
		manager.groundManager.Init();
		manager.player.Run();
	}
	public override void OnUpdate(float delta)
	{
		base.OnUpdate(delta);
		string s;
		if (stateTime < 3)
			s = ((int)(4-stateTime)).ToString();
		else
			s = "가즈아ㅏㅏㅏ!!!";

		manager.readyUI.SetDesc(s);
	}
	public override bool OnCheck(ref GameState nextState)
	{
		if (stateTime >= 4)
		{
			nextState = GameState.play;
			return true;
		}

		return false;
	}
}

public class GameStatePlay : GameStateBase
{
	public GameStatePlay(GameManager manager) : base(manager, GameState.play) { }
	public override void OnEnter()
	{
		base.OnEnter();
		manager.speed = GameValue.lineWidth * 3;
		manager.ShowPlayUI();
	}
	public override void OnUpdate(float delta)
	{
		base.OnUpdate(delta);
		if (Input.GetKeyDown(KeyCode.RightArrow))
			manager.OnClickRight();
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
			manager.OnClickLeft();
		//else if (Input.GetKeyDown(KeyCode.UpArrow))
		//	manager.speed++;
		//else if (Input.GetKeyDown(KeyCode.DownArrow))
		//	manager.speed--;

		manager.score = (int)(stateTime * manager.speed);
		manager.playUI.SetScore(manager.score);
	}
}

public class GameStateEnd : GameStateBase
{
	public GameStateEnd(GameManager manager) : base(manager, GameState.end) { }
	public override void OnEnter()
	{
		base.OnEnter();
		manager.speed = 0;
		manager.ShowEndUI();
		manager.endUI.SetDesc("End");
		manager.player.Stop();
	}
}