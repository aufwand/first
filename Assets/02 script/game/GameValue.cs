﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameValue
{
	public const float lineWidth = 2.5f;
	public const float enemyInterval = 1.5f;
	public const int initHP = 3;
}
