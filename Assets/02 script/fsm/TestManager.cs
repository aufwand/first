﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestManager : FsmManager<TestState>
{
	public bool changeState = false;
	protected override void InitializeState()
	{
		AddState(new TestStateLobby(this));
		AddState(new TestStateBattle(this));
		AddState(new TestStateEnd(this));
		state = TestState.lobby;
	}
	void Awake()
	{
		InitializeState();
	}
	void Update()
	{
		UpdateState(Time.deltaTime);
	}

	public void OnClick()
	{
		changeState = true;
	}
}
