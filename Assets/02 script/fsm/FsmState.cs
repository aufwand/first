﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FsmState<T>
{
    private T m_state;
    public T state
    {
        get { return m_state; }
        private set { m_state = value; }
    }
	protected float stateTime = 0;

	public bool selfTransition;

    public FsmState(T state)
	{
		this.state = state;
		selfTransition = false;
	}
	
	public virtual void OnEnter() { stateTime = 0; }
	public virtual bool OnCheck(ref T nextState) { return false; }
	public virtual void OnUpdate(float delta) { stateTime += delta; }
	public virtual void OnExit() { }
}
