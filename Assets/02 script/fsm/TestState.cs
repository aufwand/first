﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TestState
{
	lobby,
	battle,
	end,
}

public class TestStateBase : FsmState<TestState>
{
	protected TestManager manager;
	public TestStateBase(TestManager manager, TestState state) : base(state)
	{
		this.manager = manager;
	}
	public override void OnEnter()
	{
		base.OnEnter();
		Debug.Log(state);
	}
}

public class TestStateLobby : TestStateBase
{
	public TestStateLobby(TestManager manager) : base(manager, TestState.lobby) { }
	public override void OnEnter()
	{
		base.OnEnter();
		manager.changeState = false;
	}
	public override bool OnCheck(ref TestState nextState)
	{
		if (manager.changeState)
		{
			nextState = TestState.battle;
			return true;
		}

		return false;
	}
}
public class TestStateBattle : TestStateBase
{
	public TestStateBattle(TestManager manager) : base(manager, TestState.battle)
	{
		selfTransition = true;
	}
	public override void OnEnter()
	{
		base.OnEnter();
		manager.changeState = false;
	}
	public override bool OnCheck(ref TestState nextState)
	{
		if (manager.changeState)
		{
			nextState = TestState.end;
			return true;
		}

		return false;
	}
}
public class TestStateEnd : TestStateBase
{
	public TestStateEnd(TestManager manager) : base(manager, TestState.end) { }
	public override void OnEnter()
	{
		base.OnEnter();
		manager.changeState = false;
	}
	public override bool OnCheck(ref TestState nextState)
	{
		if (manager.changeState)
		{
			nextState = TestState.lobby;
			return true;
		}

		return false;
	}
}
