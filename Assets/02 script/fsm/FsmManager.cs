﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FsmManager<T> : MonoBehaviour
{
	private T m_state;
	public T state
	{
		get { return m_state; }
		set
		{
			if (!states.ContainsKey(value))
			{
				Debug.LogErrorFormat("[{0}]존재하지 않는 스테이트 입니다", state.ToString());
				return;
			}
			ChangeState(value);
		}
	}
	private T nextState;
	public FsmState<T> currentState;
    public Dictionary<T, FsmState<T>> states = new Dictionary<T, FsmState<T>>();

	protected abstract void InitializeState();

	protected void AddState(FsmState<T> newState)
	{
		if (newState == null)
			return;

		if (states.ContainsKey(newState.state))
		{
			Debug.LogErrorFormat("[{0}]이미 존재하는 스테이트 입니다", newState.ToString());
			return;
		}		
		states.Add(newState.state, newState);
	}

	protected void UpdateState(float delta)
	{
		if (currentState == null)
			return;

		if (currentState.OnCheck(ref nextState))
		{
			state = nextState;
		}
		else
		{
			currentState.OnUpdate(delta);
		}
	}

	private void ChangeState(T next)
	{
		if(currentState != null)
		{
			if (state.Equals(next) && !currentState.selfTransition)
			{
				Debug.LogErrorFormat("[{0}]같은 스테이트로 바꿀 수 없습니다.", state.ToString());
				return;
			}

			currentState.OnExit();
		}

		m_state = next;
		currentState = states[state];

		currentState.OnEnter();
	}
}
