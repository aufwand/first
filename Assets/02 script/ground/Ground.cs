﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
	public GameObject pref;
	public List<GameObject> enemies = new List<GameObject>();
	public GameObject enemyCenter;

	public void Init(int idx)
	{
		if (enemies == null)
			enemies = new List<GameObject>();

		ClearEnemies();

		if(idx > 0)
			SpawnEnemies();
	}
	private void ClearEnemies()
	{
		while(enemies.Count > 0)
		{
			Destroy(enemies[0].gameObject);
			enemies.RemoveAt(0);
		}
	}
	private void SpawnEnemies()
	{
		int distance = 3;
		for (int z = 0; z < 20; z+=distance)
		{
			int key = Random.Range(1, 31);
			for(int x = 0; x < 5; ++x)
			{
				bool doSpawn = key % 2 == 1;
				key /= 2;

				if (doSpawn)
				{
					GameObject enemy = Instantiate(pref);
					enemy.transform.SetParent(enemyCenter.transform);
					enemy.transform.localPosition = new Vector3(x - 2, 0, z - 9.5f);
					enemies.Add(enemy);
				}
			}
		}
	}
}
