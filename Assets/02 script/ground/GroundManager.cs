﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour
{
	private GameManager _gameManager;
	public GameManager gameManager
	{
		get
		{
			if (_gameManager == null)
				_gameManager = GameManager.me;

			return _gameManager;
		}
	}
	public List<GameObject> grounds;

	public float speed
	{
		get { return gameManager.speed; }
	}
	public void Init()
	{
		int cnt = grounds.Count;

		for(int i=0; i<cnt; ++i)
		{
			grounds[i].transform.position = new Vector3(0, 0, i * 120);
		}
	}

	void FixedUpdate()
	{
		int cnt = grounds.Count;

		float delta = Time.deltaTime;
		for(int i=0; i<cnt; ++i)
		{
			grounds[i].transform.Translate(new Vector3(0, 0, -delta * speed));
		}
		if(grounds[0].transform.position.z < -120)
		{
			GameObject g = grounds[0];
			grounds.RemoveAt(0);
			grounds.Add(g);
			g.transform.position += new Vector3(0, 0, grounds.Count * 120);
		}
	}
}
