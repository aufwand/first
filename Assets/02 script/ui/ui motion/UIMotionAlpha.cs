﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMotionAlpha : UIMotionBase
{
	public float from, to;
	private CanvasGroup canvas;

	public UIMotionAlpha(UIMotion motion, float from, float to) : base(motion)
	{
		canvas = motion.GetComponent<CanvasGroup>();

		this.from = from;
		this.to = to;
	}

	public override void InitPlay()
	{
		canvas.alpha = from;
	}
	public override void InitRewind()
	{
		canvas.alpha = to;
	}
	public override void Play()
	{
		Cancel();
		InitPlay();

		lt = LeanTween.value(from, to, time)
			.setOnUpdate((float val) =>
			{
				canvas.alpha = val;
			})
			.setRepeat(repeat);
	}
	public override void Rewind()
	{
		Cancel();
		InitRewind();

		lt = LeanTween.value(to, from, time)
			.setOnUpdate((float val) =>
			{
				canvas.alpha = val;
			})
			.setRepeat(repeat);
	}
}