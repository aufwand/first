﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIMotion))]
public class UIMotionInspector : Editor
{
	UIMotion motion;
	private void OnEnable()
	{
		motion = target as UIMotion;
	}
	public override void OnInspectorGUI()
	{
		EditorGUILayout.BeginHorizontal();
		motion.repeat = EditorGUILayout.IntField("Repeat (-1 is infinity)", motion.repeat);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		motion.time = EditorGUILayout.FloatField("Time", motion.time);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		motion.playOnEnable = EditorGUILayout.Toggle("Play on Enable", motion.playOnEnable);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		motion.alpha = EditorGUILayout.Toggle("Alpha", motion.alpha);
		EditorGUILayout.EndHorizontal();
		if (motion.alpha)
		{
			EditorGUILayout.BeginHorizontal();
			motion.alphaFrom = EditorGUILayout.FloatField("From", motion.alphaFrom);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			motion.alphaTo = EditorGUILayout.FloatField("To", motion.alphaTo);
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.BeginHorizontal();
		motion.scale = EditorGUILayout.Toggle("Scale", motion.scale);
		EditorGUILayout.EndHorizontal();
		if (motion.scale)
		{
			EditorGUILayout.BeginHorizontal();
			motion.scaleFrom = EditorGUILayout.Vector3Field("From", motion.scaleFrom);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			motion.scaleTo = EditorGUILayout.Vector3Field("To", motion.scaleTo);
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.BeginHorizontal();
		motion.move = EditorGUILayout.Toggle("Move", motion.move);
		EditorGUILayout.EndHorizontal();
		if (motion.move)
		{
			EditorGUILayout.BeginHorizontal();
			motion.moveFrom = EditorGUILayout.Vector2Field("From", motion.moveFrom);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			motion.moveTo = EditorGUILayout.Vector2Field("To", motion.moveTo);
			EditorGUILayout.EndHorizontal();
		}
	}

}
