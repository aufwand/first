﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIMotionBase
{
	protected UIMotion motion;
	protected LTDescr lt = null;
	protected int repeat
	{
		get { return motion.repeat; }
	}
	protected float time
	{
		get { return motion.time; }
	}
	public UIMotionBase(UIMotion motion)
	{
		this.motion = motion;
	}
	protected void Cancel()
	{
		if (lt != null)
		{
			LeanTween.cancel(lt.uniqueId);
			lt = null;
		}
	}
	public void OnDestroy()
	{
		Cancel();
	}
	public abstract void InitPlay();
	public abstract void InitRewind();
	public abstract void Play();
	public abstract void Rewind();
}