﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMotionMove : UIMotionBase
{
	public Vector2 from, to;
	private RectTransform rect;

	public UIMotionMove(UIMotion motion, Vector2 from, Vector2 to) : base(motion)
	{
		rect = motion.transform as RectTransform;

		this.from = from;
		this.to = to;
	}

	public override void InitPlay()
	{
		rect.anchoredPosition = from;
	}
	public override void InitRewind()
	{
		rect.anchoredPosition = to;
	}
	public override void Play()
	{
		Cancel();
		InitPlay();

		lt = LeanTween.value(motion.gameObject, from, to, time)
			.setOnUpdate((Vector2 val) =>
			{
				rect.anchoredPosition = val;
			})
			.setRepeat(repeat);
	}
	public override void Rewind()
	{
		Cancel();
		InitRewind();

		lt = LeanTween.value(motion.gameObject, to, from, time)
			.setOnUpdate((Vector2 val) =>
			{
				rect.anchoredPosition = val;
			})
			.setRepeat(repeat);
	}
}