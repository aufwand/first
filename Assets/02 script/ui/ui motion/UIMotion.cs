﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMotion : MonoBehaviour
{
	public int repeat = 1;
	public float time = 0.5f;
	public bool playOnEnable = false;
	public bool alpha;
	public float alphaFrom = 0, alphaTo = 1;
	public bool scale;
	public Vector3 scaleFrom = Vector3.zero, scaleTo = Vector3.one;
	public bool move;
	public Vector2 moveFrom = Vector2.zero, moveTo = Vector2.zero;

	private UIMotionAlpha alphaMotion;
	private UIMotionScale scaleMotion;
	private UIMotionMove moveMotion;

	private void Awake()
	{
		if (alpha)
			alphaMotion = new UIMotionAlpha(this, alphaFrom, alphaTo);

		if (scale)
			scaleMotion = new UIMotionScale(this, scaleFrom, scaleTo);

		if (move)
			moveMotion = new UIMotionMove(this, moveFrom, moveTo);
	}
	private void OnEnable()
	{
		if (playOnEnable)
			Play();
	}
	public void OnDestroy()
	{
		if(alphaMotion != null)
			alphaMotion.OnDestroy();
		if(scaleMotion != null)
			scaleMotion.OnDestroy();
		if (moveMotion != null)
			moveMotion.OnDestroy();
	}
	public void Play()
	{
		if (alphaMotion != null)
			alphaMotion.Play();
		if (scaleMotion != null)
			scaleMotion.Play();
		if (moveMotion != null)
			moveMotion.Play();
	}
	public void Rewind()
	{
		if (alphaMotion != null)
			alphaMotion.Rewind();
		if (scaleMotion != null)
			scaleMotion.Rewind();
		if (moveMotion != null)
			moveMotion.Rewind();
	}
}