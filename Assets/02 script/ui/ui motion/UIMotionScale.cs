﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMotionScale : UIMotionBase
{
	public Vector3 from, to;
	private Transform transform;

	public UIMotionScale(UIMotion motion, Vector3 from, Vector3 to) : base(motion)
	{
		transform = motion.transform;

		this.from = from;
		this.to = to;
	}

	public override void InitPlay()
	{
		transform.localScale = from;
	}
	public override void InitRewind()
	{
		transform.localScale = to;
	}
	public override void Play()
	{
		Cancel();
		InitPlay();

		lt = LeanTween.value(motion.gameObject, from, to, time)
			.setOnUpdate((Vector3 val) =>
			{
				transform.localScale = val;
			})
			.setRepeat(repeat);
	}
	public override void Rewind()
	{
		Cancel();
		InitRewind();

		lt = LeanTween.value(motion.gameObject, to, from, time)
			.setOnUpdate((Vector3 val) =>
			{
				transform.localScale = val;
			})
			.setRepeat(repeat);
	}
}
