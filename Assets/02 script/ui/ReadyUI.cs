﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadyUI : MonoBehaviour
{
	public Text desc;
	public void SetActive(bool val)
	{
		gameObject.SetActive(val);
	}
	public void SetDesc(string msg)
	{
		desc.text = msg;
	}
}
