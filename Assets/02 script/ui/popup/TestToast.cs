﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestToast : PopupBase
{
	public Text title;
	public Text desc;

	private float life;
	private bool closing;

	public static void Show(string title, string desc, float life = 2f)
	{
		TestToast p = PopupLayer.Show<TestToast>("test toast");
		p.Init(title, desc, life);
	}

	private void Init(string title, string desc, float life)
	{
		this.title.text = title;
		this.desc.text = desc;
		this.life = life;
		closing = false;
		motion.Play();
	}
	private void FixedUpdate()
	{
		if (life > 0)
			life -= Time.deltaTime;
		else if (!closing)
		{
			closing = true;
			Close();
		}	
	}
}
