﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TestPopup : PopupBase
{
	public Text title;
	public Text desc;

	public Action onOk, onClose;

	public static void Show(string title, string desc, Action onOk, Action onClose)
	{
		TestPopup p = PopupLayer.Show<TestPopup>("test popup");
		p.Init(title, desc, onOk, onClose);
	}

	private void Init(string title, string desc, Action onOk, Action onClose)
	{
		this.title.text = title;
		this.desc.text = desc;

		this.onOk = onOk;
		this.onClose = onClose;
		motion.Play();
	}
	
	public void OnClickOk()
	{
		if (onOk != null)
			onOk();

		Close();
	}
	public void OnClickClose()
	{
		if (onClose != null)
			onClose();

		Close();
	}
}
