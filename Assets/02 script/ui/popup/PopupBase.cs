﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupBase : MonoBehaviour
{
	public UIMotion motion;
	public virtual void Close()
	{
		motion.Rewind();
		LeanTween.delayedCall(motion.time, () =>
		{
			Destroy(gameObject);
		});
	}
}
