﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PopupLayer : MonoBehaviour
{
	private static PopupLayer _me;
	public static PopupLayer me
	{
		get { return _me; }
		private set { _me = value; }
	}
	public void OnEnable()
	{
		_me = this;
	}
	public void OnDisable()
	{
		_me = null;
	}

	public static T Show<T>(string name) where T : PopupBase
	{
		string path = "popup/" + name;

		GameObject go = Resources.Load<GameObject>(path);

		T popup = Instantiate(go).GetComponent<T>();

		popup.transform.SetParent(me.transform);		
		popup.transform.SetAsLastSibling();
		popup.transform.localPosition = Vector3.zero;		

		return popup;
	}

	public void OnClickPopup()
	{
		TestPopup.Show("Test Popup", "Test Popup Desc", null, null);
	}
	public void OnClickToast()
	{
		TestToast.Show("Test Toast", "Test Toast Desc");
	}
}
