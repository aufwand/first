﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayUI : MonoBehaviour
{
	public Text score;
	public Text hp;
	public void SetActive(bool val)
	{
		gameObject.SetActive(val);
	}
	public void SetScore(int score)
	{
		this.score.text = score.ToString();
	}
	public void SetHP(int hp)
	{
		this.hp.text = hp.ToString();
	}
}
