﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	private GameManager _gameManager;
	public GameManager gameManager
	{
		get
		{
			if (_gameManager == null)
				_gameManager = GameManager.me;

			return _gameManager;
		}
	}
	private Animator m_animator;
	public Animator animator
	{
		get
		{
			if (m_animator == null)
				m_animator = GetComponent<Animator>();
			return m_animator;
		}
	}
	private void OnEnable()
	{
		animator.SetInteger("state", 1);
		transform.eulerAngles = new Vector3(0, 180, 0);
	}
	private void Update()
	{
		transform.Translate(0, 0, Time.deltaTime * gameManager.speed);
		if (transform.position.z < -20)
			gameObject.SetActive(false);
	}
}
