﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	private GameManager _gameManager;
	public GameManager gameManager
	{
		get
		{
			if (_gameManager == null)
				_gameManager = GameManager.me;

			return _gameManager;
		}
	}
	private Animator m_animator;
	public Animator animator
	{
		get
		{
			if (m_animator == null)
				m_animator = GetComponent<Animator>();
			return m_animator;
		}
	}
	private BoxCollider m_collider;
	private BoxCollider collider
	{
		get
		{
			if (m_collider == null)
				m_collider = GetComponent<BoxCollider>();
			return m_collider;
		}
	}
	private Material material;
	private int m_hp = 3;
	public int hp
	{
		get { return m_hp; }
		set
		{
			m_hp = value;
			gameManager.playUI.SetHP(m_hp);
		}
	}
	private int m_position = 0;
	public int position
	{
		get { return m_position; }
		set
		{
			m_position = value;
			transform.position = new Vector3(position * GameValue.lineWidth, 0, 0);
		}
	}
	public void Init()
	{
		material = GetComponentInChildren<SkinnedMeshRenderer>().material;
		hp = GameValue.initHP;
		position = 0;
		Stop();
	}
	public void Stop()
	{
		animator.SetInteger("state", 0);
	}
	public void Run()
	{
		animator.SetInteger("state", 1);
	}

	public void MoveRight()
	{
		if (position >= 2)
			return;
		else
		{
			position++;
		}	
	}
	public void MoveLeft()
	{
		if (position <= -2)
			return;
		else
		{
			position--;
		}	
	}
	private void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.gameObject.name);
		hp--;

		if(hp == 0)
			gameManager.EndGame();
		else
		{
			collider.enabled = false;
			LeanTween.value(1, -1, 1)
				.setOnUpdate((float val) => 
				{
					material.SetFloat("_Alpha", Mathf.Abs(val));
				})
				.setRepeat(3)
				.setOnComplete(()=>
				{
					collider.enabled = true;
				});
		}
	}
}
